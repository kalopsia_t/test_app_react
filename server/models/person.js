import mongoose from 'mongoose';

const PersonSchema = new mongoose.Schema({
	name : { type : String },
	surname : { type : String },
	phone : { type : String },
	gender : { type : Boolean },
	age : { type : Number }
});

const Person = mongoose.model('Person',PersonSchema);
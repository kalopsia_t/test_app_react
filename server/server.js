import express from 'express';
import bodyParser from 'body-parser';

import { SERVER_PORT } from "./utils/constants";
import * as services from './utils/services';

services.connectToDB();

const app = express();

app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/persons',(req,res) => {
	services
		.getPersons()
		.then(data => res.send(data));
});

app.post('/persons',(req,res) => {
	services
		.createPerson(req.body)
		.then(data => res.send(data));
});

app.delete('/persons/:id',(req,res) => {
	services
		.deletePerson(req.params.id)
		.then(data => res.send(data));
});

const server = app.listen(SERVER_PORT,() => {
	console.log(`Server running now on port : ${SERVER_PORT}`);
});
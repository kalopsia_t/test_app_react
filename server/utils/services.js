import mongoose from 'mongoose';

import '../models/person';

const Person = mongoose.model('Person');

export function connectToDB(){
	mongoose.connect(`mongodb://localhost/person`);
};

export function getPersons(){
	return Person.find();
}

export function createPerson(data){
	const person = Person({
		name : data.name,
		surname : data.surname,
		phone : data.phone,
		gender : data.gender,
		age : data.age
	});

	return person.save();
}

export function deletePerson(id){
	return Person.findById(id).remove();
}
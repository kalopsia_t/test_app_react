import { FETCH_PERSONS , ADD_PERSON , DELETE_PERSON } from '../constants/types';

const initialState = {
	data : 'unload'
};

export default function persons(state = initialState,action) {
	switch(action.type){
		case FETCH_PERSONS : 
			return {...state, data : action.data }
		default : 
			return state;
	}
}
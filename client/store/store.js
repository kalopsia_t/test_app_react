import { createStore , applyMiddleware } from 'redux'
import thunk from 'redux-thunk';
import mainReducer from '../reducers/MainReducer'

export default function configureStore(initialState) {
  const store = createStore(mainReducer, initialState, applyMiddleware(thunk))

  if (module.hot) {
    module.hot.accept('../reducers/MainReducer', () => {
      const nextMainReducer = require('../reducers/MainReducer')
      store.replaceReducer(nextMainReducer)
    })
  }

  return store
}
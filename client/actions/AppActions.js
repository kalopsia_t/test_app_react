import { FETCH_PERSONS , ADD_PERSON , DELETE_PERSON } from '../constants/types';
import axios from 'axios';

function fetchPersons(data){
	return {
		type : FETCH_PERSONS,
		data
	}
}

export function fetchPersonsRequest(){
	return dispatch => {
		return axios.get(`http://localhost:8080/persons`)
			.then((res) => {
				console.log(FETCH_PERSONS);
				dispatch(fetchPersons(res.data));
			})
	}
}

function addPerson(data){
	return {
		type : ADD_PERSON,
		data
	}
}

export function addPersonRequest(data){
	return dispatch => {
		return axios.post(`http://localhost:8080/persons`,data)
			.then((res) => {
				dispatch(addPerson(res.data))
			})
	}
}

function addPerson(data){
	return {
		type : DELETE_PERSON,
		data
	}
}

export function deletePersonRequest(id){
	return dispatch => {
		return axios.delete(`http://localhost:8080/persons/${id}`)
			.then((res) => {
				dispatch(addPerson(res.data))
			})
	}
}


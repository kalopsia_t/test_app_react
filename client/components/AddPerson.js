import React , { Component } from 'react';
import { Modal, Button , FormGroup , Radio} from 'react-bootstrap';
import FieldGroup from './FieldGroup';
import { phone_regexp } from '../constants/phone_regexp';

const initialState = {
	show : false,
	gender : [false,false],
	validation : {
		first:null,
		last: null,
		phone:null,
		age:null,
		gender:null
	}
}


export default class AddPerson extends Component {
	constructor(props) {
		super(props);
		this.state = initialState;
		this.data = {
			name : '',
			surname : '',
			phone : '',
			age : '',
			gender: ''
		}
	}

	triggerModal(data){
		this.setState({show:data});
	}

	onChange(e,type){
		if(type === 'first_name'){
			this.data.name = e.value;
		} else if(type === 'last_name'){
			this.data.surname = e.value;
		} else if(type === 'phone'){
			this.data.phone = e.value;
		} else if(type === 'age'){
			this.data.age = e.value;
		}
	}
	validation(){
		let isValid = true;
		if(this.data.name === ''){
			let newState = this.state;
			newState.validation.first = 'error';
			this.setState({...newState});
			isValid = false;
		}
		if(this.data.surname === ''){
			let newState = this.state;
			newState.validation.last = 'error';
			this.setState({...newState});
			isValid = false;
		}
		if(!phone_regexp.test(this.data.phone)){
			let newState = this.state;
			newState.validation.phone = 'error';
			this.setState({...newState});
			isValid = false;
		}
		if(!(/^[0-9]+$/.test(this.data.age))){
			let newState = this.state;
			newState.validation.age = 'error';
			this.setState({...newState});
			isValid = false;
		}
		let gender = true;
		if(this.state.gender[0]){
			gender = false;
		}
		if(this.state.gender[1]){
			gender = false;
		}
		if(gender){
			let newState = this.state;
			newState.validation.gender = 'error';
			this.setState({...newState});
			isValid = false;
		}
		return isValid;
	}
	onSubmit(){
		let isValid = this.validation();
		
		if(isValid){
			if(this.state.gender[0]){
				this.data.gender = true;
			}
			if(this.state.gender[1]){
				this.data.gender = false;
			}
			this.data.age = parseFloat(this.data.age);
			this.props.addPerson(this.data).then(()=>{
				this.props.fetchPersons();
			});
			this.triggerModal(false);
			this.data = {
				name : '',
				surname : '',
				phone : '',
				age : '',
				gender: ''
			}
			this.setState(initialState);
		}
	

		console.log(this.data);
	}

	render(){

		return (
			<div className="modal-container" style={{height: 200}}>
		        <Button
		          bsStyle="primary"
		          bsSize="large"
		          onClick={e => this.triggerModal(true)}
		        >
		          Add Person
		        </Button>

		        <Modal
		          show={this.state.show}
		          onHide={() => this.triggerModal(false)}
		          container={this}
		          aria-labelledby="contained-modal-title"
		        >
          		<Modal.Header closeButton>
            		<Modal.Title id="contained-modal-title">Add new person</Modal.Title>
          		</Modal.Header>
          		<Modal.Body>
          			<form>
	           			<FieldGroup
	      					id="formControlsText"
	      					type="text"
	      					label="First name"
	      					placeholder="Enter your name"
	      					validate={this.state.validation.first}
	      					help={this.state.validation.first === 'error' ? 'Please enter your first name' : ''}
	      					onChange={ e => this.onChange(e.currentTarget,'first_name') }
	    				/>
	    				<FieldGroup
	      					id="formControlsText"
	      					type="text"
	      					label="Last name"
	      					placeholder="Enter your surname"
	      					validate={this.state.validation.last}
	      					help={this.state.validation.last === 'error' ? 'Please enter your last name' : ''}
	      					onChange={ e => this.onChange(e.currentTarget,'last_name') }
	    				/>
	    				<FieldGroup
					    	id="formControlsEmail"
					    	type="text"
					    	label="Email address"
					    	placeholder="Enter your email"
					    	validate={this.state.validation.phone}
					    	help={this.state.validation.phone === 'error' ? 'Please enter your last phone' : ''}
					    	onChange={ e => this.onChange(e.currentTarget,'phone') }
					    />
	    				<FormGroup validationState={this.state.validation.gender}>
    						<Radio inline
    							onClick={
    								e => {
    									this.setState({gender : [!this.state.gender[0],false]});
    								}
    							}
    							checked={this.state.gender[0]}>

        						Male
     						</Radio>
      							{' '}
					      	<Radio inline
					      		onClick={
    								e => {
    									this.setState({gender : [false,!this.state.gender[1]]});
    								}
    							}
					      		checked={this.state.gender[1]}>
					        	Female
					    	</Radio>
					    </FormGroup>
					    <FieldGroup
	      					id="formControlsText"
	      					type="text"
	      					label="Age"
	      					placeholder="Enter your age"
	      					validate={this.state.validation.age}
	      					help={this.state.validation.age === 'error' ? 'Please enter your last age' : ''}
	      					onChange={ e => this.onChange(e.currentTarget,'age') }
	    				/>
    				</form>
          		</Modal.Body>
          		<Modal.Footer>
           			<Button onClick={e => this.onSubmit()}>Add</Button>
          		</Modal.Footer>
        	</Modal>
    	</div>
		);
	}
}
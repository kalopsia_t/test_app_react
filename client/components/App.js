import React , { Component } from 'react';
import { Grid, Row , Col } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'

import AddPerson from './AddPerson';
import TablePersons from './TablePersons';
import { fetchPersonsRequest , addPersonRequest , deletePersonRequest } from './../actions/AppActions'
import './../styles/app.less';

class App extends Component {
	componentWillMount() {
		this.props.fetchPersonsRequest();
	}
	render(){
		return (
			<Grid>
				<AddPerson 
					addPerson={this.props.addPersonRequest} 
					fetchPersons={this.props.fetchPersonsRequest}/>
				<TablePersons 
					persons={this.props.persons} 
					delete={this.props.deletePersonRequest} 
					fetchPersons={this.props.fetchPersonsRequest}/>
			</Grid>
		);
	}
}

function mapStateToProps(state) {
	return {
    	persons: state
	}
}

function mapDispatchToState(dispatch){
	return bindActionCreators({
		fetchPersonsRequest,
		addPersonRequest,
		deletePersonRequest
	},dispatch);
}

export default connect(mapStateToProps,mapDispatchToState)(App)
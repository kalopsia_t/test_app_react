import React , { Component } from 'react';
import { FormGroup , ControlLabel , FormControl , HelpBlock } from 'react-bootstrap';

export default function FieldGroup({ id, label, help, validate, ...props }) {
  return (
    <FormGroup controlId={id} validationState={validate}>
      <ControlLabel>{label}</ControlLabel>
      <FormControl {...props} />
      {help && <HelpBlock>{help}</HelpBlock>}
    </FormGroup>
  );
}
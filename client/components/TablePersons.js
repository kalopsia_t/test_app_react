import React , { Component } from 'react';
import { Table , Modal , Button } from 'react-bootstrap';

export default class TablePersons extends Component {
	constructor(props){
		super(props);

		this.state = {
			show : false,
			id : ''
		}	
	}
	openDeleteModal(id,data){
		this.setState({show:data,id:id});
	}

	closeDeleteModal(){
		this.setState({
			show : false,
			id : ''
		})
	}

	deletePerson(e){
		this.props.delete(this.state.id).then(() => {
			this.props.fetchPersons()
			this.closeDeleteModal();
		});
		console.log(this.state);
		//this.triggerModal(false);
	}
	render(){
		let persons = this.props.persons.data;
		return (
			<div>
			{persons !== 'unload' ? <Table striped bordered condensed hover>
			   <thead>
			      <tr>
			        <th>#</th>
			        <th>First Name</th>
			        <th>Last Name</th>
			        <th>Phone</th>
			        <th>Gender</th>
			        <th>Age</th>
			      </tr>
			    </thead>
			    <tbody>
			    {persons.map((item,index) => {
			    	return <tr id={item._id} key={index} onClick={e => this.openDeleteModal(item._id,true)}>
			        <td>{index}</td>
			        <td>{item.name}</td>
			        <td>{item.surname}</td>
			        <td>{item.phone}</td>
			        <td>{item.gender ? "Male" : 'Female'}</td>
			        <td>{item.age}</td>
			      </tr>
			    })}
			    </tbody>
			    <Modal {...this.props} show={this.state.show} onHide={() => this.closeDeleteModal()} dialogClassName="custom-modal">
          			<Modal.Header closeButton>
            			<Modal.Title id="contained-modal-title-lg">Person settings</Modal.Title>
          			</Modal.Header>
          			<Modal.Body>
		        		<h4>Setings</h4>
		        		<p>You can delete this person</p>
          			</Modal.Body>
          			<Modal.Footer>
				        <Button onClick={e => this.deletePerson()}>Delete</Button>
				    </Modal.Footer>
        		</Modal>
			</Table> : (<span>Fetching...</span>)
		}
		</div>
		);
	}
}